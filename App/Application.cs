using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MusicKeyCalculator.ExtensionMethods;
using MusicKeyCalculator.Services;
using static MusicKeyCalculator.Models.ScaleSet;
using static MusicKeyCalculator.Services.ProfilesGenerator;

namespace MusicKeyCalculator.App
{
    public class Application
    {
        public Dictionary<ScaleType, IDictionary<string, decimal>> Profiles {get; set;}

        public IServiceProvider ServiceProvider {get; set;}

        public MidiReader MidiReader {get; set;}

        public Application(IServiceProvider serviceProvider, MidiReader midiReader) 
        {
            ServiceProvider = serviceProvider;
            MidiReader = midiReader;
            Profiles = ProfilesGenerator.Profiles;
        }

        public void Run(string[] args)
        {
            if (args.Count() != 1) {
                return;
            }

            MidiReader.ReadAndCountMidi(args[0]);
        }
    }
}