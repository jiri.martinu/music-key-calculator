using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace MusicKeyCalculator.ExtensionMethods
{
    public static class MyExtensions
    {
        public static string ToHumanReadable(this decimal number)
        {
            return number.ToString("f99").TrimEnd('0');
        }

        public static decimal ToDecimal(this string number)
        {
            CultureInfo cultureInfo = number.Contains(",") ? CultureInfo.CurrentCulture : CultureInfo.InvariantCulture;

            return decimal.Parse(number, cultureInfo);
        }

        public static string GetTimestamp(this DateTime value) {
            return value.ToString("yyyyMMddHHmmssffff");
        }

        public static IEnumerable<T> GetEnumValues<T>() {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        public static int SumBytesInArrayAsOneNumber(this byte[] inputBytes)
        {
            List<byte> bytes = new List<byte>(inputBytes);

            bytes.Reverse();

            int count = 4 - (bytes.Count() % 4);

            if (count != 4) {
                bytes.InsertRange(bytes.Count(), Enumerable.Repeat((byte) 0, count));
            }

            return BitConverter.ToInt32(bytes.ToArray(), 0);
        }

        public static long SumBytesInArrayAsOneNumberVLQ(this byte[] inputBytes)
        {
            Queue<byte> queue = new Queue<byte>(inputBytes);
            long result;
            byte current;

            if (((result = (int) queue.Dequeue()) & 0x80) != 0x00) { 
                result &= 0x7F;

                do {
                    result = (result << 7) + ((current = queue.Dequeue()) & 0x7F);
                } while ((current & 0x80) != 0x00);
            }

            return result;
        }
    }   
}