using System.Collections.Generic;
using System.Linq;
using MusicKeyCalculator.ExtensionMethods;

namespace MusicKeyCalculator.Models
{
    public class Message
    {
        public enum MessageType {Event, Note};

        public enum MessagePhase {Ticks, Body, End};

        public MessageType Type {get; set;}

        public MessagePhase Phase {get; set;} = MessagePhase.Ticks;

        public long StartTicks {get; set;} = 0;

        public IList<byte> Ticks {get; set;} = new List<byte>();

        public IList<byte> Body {get; set;} = new List<byte>();

        public void AddByte(byte input)
        {
            switch (Phase) {
                case MessagePhase.Ticks:
                    Ticks.Add(input);
                    break;
                case MessagePhase.Body:
                    Body.Add(input);
                    break;
            }
        }

        public long GetTicks()
        {
            return Ticks.ToArray().SumBytesInArrayAsOneNumberVLQ();
        }

        public Note GetNote()
        {
            if (Type != MessageType.Note) {
                return null;
            }

            return new Note((int) Body[0], (int) Body[1], (int) Body[2], StartTicks);
        }
    }
}