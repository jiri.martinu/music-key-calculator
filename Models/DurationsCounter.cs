using System.Collections.Generic;
using System.Linq;
using MusicKeyCalculator.ExtensionMethods;

namespace MusicKeyCalculator.Models
{
    public class DurationsCounter
    {
        public IDictionary<string, long> Counts {get; set;} = new Dictionary<string, long>();

        public DurationsCounter() 
        {
            foreach (string note in Note.Names) {
                Counts.Add(note, 0);   
            }
        }
    }
}